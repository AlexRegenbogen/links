# Git
- https://sethrobertson.github.io/
- https://git-scm.com/book/en/v2/Distributed-Git-Contributing-to-a-Project
- http://who-t.blogspot.nl/2009/12/on-commit-messages.html
- https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows
- http://guides.beanstalkapp.com/version-control/branching-best-practices.html

# Orchestration
- https://github.com/ramitsurana/awesome-kubernetes